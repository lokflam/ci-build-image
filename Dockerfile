FROM alpine:3.14

ENV PATH="/.tfenv/bin:${PATH}"

RUN apk add --no-cache \
  bash \
  build-base \
  ca-certificates \
  curl \
  git \
  gnupg \
  make \
  openssh-client \
  jq \
  jsonnet \
  unzip && update-ca-certificates

RUN git clone https://github.com/tfutils/tfenv.git /.tfenv
